# playwright-parameters

Really quick test to check parameter transmission using environment variables.

The following ones are needed : 

- TC_CUF_pw_tc : `testcase cuf`

- IT_CUF_pw_it : `iteration cuf`

- CPG_CUF_pw_cpg : `campaign cuf`

- TS_CUF_pw_ts : `testsuite cuf`

- DS_first_comparison : `L'eau minérale (naturelle) est une catégorie # d'eau, dont les caractéristiques* sont définies réglementairement.`
- DS_second_comparison : `La taux - de conversion := "€" [euros] / ${dollars} @ugmente; & ça continue`
- DS_third_comparison : `Les taux _ des prêts sont entre ~ 3 \ ^4 %pourcents% | ils ont <augmentés> ?!?!`
