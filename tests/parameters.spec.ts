import { test, expect } from '@playwright/test';

test('check cufs', async () => {
  expect(process.env.TC_CUF_pw_tc).toEqual("testcase cuf");
  expect(process.env.IT_CUF_pw_it).toEqual("iteration cuf");
  expect(process.env.CPG_CUF_pw_cpg).toEqual("campaign cuf");
  expect(process.env.TS_CUF_pw_ts).toEqual("testsuite cuf");
});

test('check dataset', async () => {
    expect(process.env.DS_first_comparison).toEqual("L'eau minérale (naturelle) est une catégorie # d'eau, dont les caractéristiques* sont définies réglementairement.");
    expect(process.env.DS_second_comparison).toEqual('La taux - de conversion := "€" [euros] / ${dollars} @ugmente; & ça continue');
    expect(process.env.DS_third_comparison).toEqual('Les taux _ des prêts sont entre ~ 3 \\ ^4 %pourcents% | ils ont <augmentés> ?!?!');
});
